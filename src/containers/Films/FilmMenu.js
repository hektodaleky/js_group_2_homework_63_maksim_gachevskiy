import React, {Component, Fragment} from "react";
import axios from "axios";
import AddTask from "../AddTask/AddTask";
import "../TasksMenu/TasksMenu.css";
import FilmList from "../../components/FilmList/FilmList";
class FilmMenu extends Component {
    state = {
        tasks: [],
    };
    loadItems = (url) => {
        axios.get(url).then(response => {
            const tasks = [];
            console.log(response.data);

            for (let key in response.data) {
                tasks.push({...response.data[key], key})
            }
            this.setState({tasks});
        })
    };

    itemAddedHandler = () => {
        this.loadItems('/films.json');
    };


    componentDidMount() {
        this.loadItems('/films.json');


    };

    removeTask = (id) => {
        axios.delete(`/films/${id}.json`).then(() => {
            this.loadItems('/films.json');
        });
    };

    changeTask = (id) => {
        const index = this.state.tasks.findIndex(element => element.key === id);
        console.log(this.state.tasks[index]);
        const val=this.state.tasks[index].title;
        axios.put(`/films/${id}.json`, {id:this.state.tasks[index].id,title:this.state.tasks[index].title})
        console.log(val,"qw")
    };

    componetDidUpdate() {
        console.log("das");
    };

    changeValue = (event, id) => {
        const index = this.state.tasks.findIndex(element => element.id === id);
        const myFilm = [...this.state.tasks];
        myFilm[index].title = event.target.value;
        this.setState({tasks: myFilm});
        console.log(myFilm);




    };

    render() {

        return (
            <Fragment>
                <p className="warning">Для применения изменений нажмите "Change"</p>
                <div className="TasksMenu">{this.state.tasks.map((oneTask) => {
                    return (




                        <FilmList changeFilm={() => this.changeTask(oneTask.key)}
                                  changer={(event) => {
                                      this.changeValue(event, oneTask.id)
                                  }}
                                  title={oneTask.title}
                                  key={oneTask.id}
                                  remove={() => {
                            this.removeTask(oneTask.key)
                        }}

                        />

                    );

                })}
                </div>
                <AddTask url="films.json" itemAdded={this.itemAddedHandler}/></Fragment>)
    }
}
export default FilmMenu;