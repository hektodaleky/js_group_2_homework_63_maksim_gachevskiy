/**
 * Created by Max on 17.01.2018.
 */
import React, {Component} from "react";
import axios from "axios";
import './AddTask.css';
class AddTask extends Component {


    state = {
        task: {
            title: '',
            id: ''
        },
        loading: false
    };

    taskCreateHandler = event => {
        event.preventDefault();
        this.setState({
            loading: true, task: {
                title: '',
                id: ''
            }
        });
        axios.post(this.props.url, this.state.task).then(response => {
            this.setState({
                loading: false
            });
            this.props.itemAdded();


        });

    };
    taskValueChanged = event => {
        const task = {
            title: event.target.value,
            id: Date.now()
        };
        this.setState({task})
    };

    render() {
        return (
            <form className="AddTask">
                <input placeholder="Введите задачу" id="addTextInput" value={this.state.task.title}
                       onChange={this.taskValueChanged} type="text"/>
                <button onClick={this.taskCreateHandler}>Add</button>
            </form>
        );
    };
}

export default AddTask;