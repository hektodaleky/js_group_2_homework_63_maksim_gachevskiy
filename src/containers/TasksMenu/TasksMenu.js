import React, {Component, Fragment} from "react";
import Task from "../../components/Task/Taks";
import axios from "axios";
import AddTask from "../AddTask/AddTask";
import "./TasksMenu.css";
import Spinner from "../../components/UI/Spinner/Spinner";
class TasksMenu extends Component {
    state = {
        tasks: [],
        loading: false
    };
    loadItems = (url) => {
        this.setState({loading: true});
        axios.get(url).then(response => {
            const tasks = [];
            console.log(response.data)

            for (let key in response.data) {
                tasks.push({...response.data[key], key})
            }
            this.setState({tasks: tasks, loading: false});
        });
        this.setState({loading: false});
    };

    itemAddedHandler = () => {
        this.loadItems('/tasks.json');
    };


    componentDidMount() {
        this.loadItems('/tasks.json');


    };

    removeTask = (id) => {
        axios.delete(`/tasks/${id}.json`).then(() => {
            this.loadItems('/tasks.json');
        });
    };

    componetDidUpdate() {
        console.log("das");
    };

    render() {
        let tasks;
        if (this.state.loading) {
            tasks = <Spinner />;

        }
        else tasks = (<div className="TasksMenu">{this.state.tasks.map((oneTask) => {
            return (




                <Task title={oneTask.title}
                      key={oneTask.id} remove={() => {
                    this.removeTask(oneTask.key)
                }}

                />

            );

        })}
        </div>);


        return (

            <Fragment>
                {tasks}
                <AddTask url="tasks.json" itemAdded={this.itemAddedHandler}/></Fragment>)
    }
}

export default TasksMenu;
