import React, {Component,Fragment} from "react";
import TasksMenu from "./containers/TasksMenu/TasksMenu";
import {Route, Switch} from "react-router-dom";
import NavLink from "react-router-dom/es/NavLink";
import FilmMenu from "./containers/Films/FilmMenu";
import './App.css'

class App extends Component {
    render() {
        return (
            <Fragment>
                <ul className="navigation">
                    <li><NavLink className="nav" to="mytasks" exact>Tasks</NavLink></li>
                    <li><NavLink className="nav" to="myfilms" exact>Films</NavLink></li>
                </ul>
                <Switch>
                    <Route path='/mytasks' exact component={TasksMenu}/>
                    <Route path='/myfilms' exact component={FilmMenu}/>
                    <Route render={() => <h1>Not Found</h1>}/>

                </Switch>
            </Fragment>
        );
    }
}

export default App;
