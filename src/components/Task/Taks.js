import React from "react";
import './Task.css';
const Task = props => {

    return(
    <div className="Task">
        <h6>{props.title}</h6>
        <p onClick={props.remove}>X</p>
    </div>)

};
export default Task;